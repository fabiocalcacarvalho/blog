from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
# Create your views here.
from django.http import HttpResponse
from .temp_data import post_data
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Post
from django.views import generic

class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/index.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

class PostCreateView(generic.CreateView):
    model = Post
    template_name = 'posts/create.html'
    fields = ['title', 'content', 'image_url']

class PostUpdateView(generic.UpdateView):
    model = Post
    template_name = 'posts/update.html'
    fields = ['title', 'content', 'image_url']

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    success_url = reverse_lazy('posts:index')

def search_posts(request):
  context = {}
  if request.GET.get('query', False):
    search_term = request.GET['query'].lower()
    post_list = Post.objects.filter(name__icontains=search_term)
    context = {"post_list": post_list}
  return render(request, 'posts/search.html', context)