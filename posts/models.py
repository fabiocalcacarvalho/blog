from django.db import models
from django.conf import settings
from django.urls import reverse

class Post(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=2047)
    image_url = models.URLField(max_length=200, null=True)
    def get_absolute_url(self):
            return reverse('posts:detail', kwargs={'pk': self.pk})
    def __str__(self):
        return f'{self.title} ({self.date})'