from django import forms

class MovieForm(forms.Form):
    title = forms.CharField(label='Título', max_length=100)
    content = forms.CharField(label='Título', max_length=2047)
    date = forms.DateTimeField(auto_now_add=True)
    image_url = forms.URLField(label='URL da imagem', max_length=100)